Esse repositório tem o objetivo de fazer testes em APIs que utilizem o oAuth2 como padrão de autenticação.

---
## Requisitos

1. PHP 5.6 >=
2. Composer

---
## Passos para rodar

Siga os passos abaixo para testar a sua API:

1. Faça o clone desse projeto
2. Com o terminal entre na pasta e execute o comando "composer install"
3. Substitua os dados de acesso a API no início do arquivo index.php