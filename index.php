<?php

require "vendor/autoload.php";

$url_token = '';
$url_api = '';
$client_id = '';
$client_secret = '';
$username = '';
$password = '';

$client = new GuzzleHttp\Client;

try {
    $response = $client->post($url_token, [
        'form_params' => [
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'password',
            'username' => $username,
            'password' => $password,
            'scope' => '*',
        ],
        'verify' => false
    ]);

    // You'd typically save this payload in the session
    $auth = json_decode((string)$response->getBody());

    $response = $client->get($url_api, [
        'headers' => [
            'Authorization' => 'Bearer ' . $auth->access_token,
        ]
    ]);

    $result = json_decode((string)$response->getBody());

    print_r($result);

} catch (GuzzleHttp\Exception\BadResponseException $e) {
    echo "Unable to retrieve access token.";
}